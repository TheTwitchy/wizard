# How to Level Up
Killed some goblins? Disarmed that trap? Seduced that withered Banshee? Congratulations, you have enough experience to level up! Use this page as a reference whenever you need to advance your level. 

## Leveling Up an Existing Class
1. Find the new features granted by the class level you are taking. This can be found in the Players Handbook, or online, and should include your new proficiency bonus, class features, and spells.

### Advance Class Level
1. Navigate to the `Actors Directory` tab and open your character sheet:

![](/imgs/lu-2.png)

2. Click the `Features` tab of your character:

![](/imgs/lu-3.png)

3. Click the `Edit Item` button of your class level:

![](/imgs/lu-4.png)

4. Go to the `Details` tab:

![](/imgs/lu-5.png)

5. Increment the `Class Level` field:

![](/imgs/lu-6.png)

6. Note your `Hit Die` type and close the class window:

![](/imgs/lu-7.png)

### Proficiency Bonus
1. If your proficiency bonus rises, be sure to reflect it in your character sheet:

![](/imgs/lu-pb-1.png) 

### Increase Hit Points
1. Roll your `Hit Die` from earlier in the chat, adding your `CON` modifier. If your `Hit Die` roll is less than half, take half automatically, adding your `CON` modifier:

![](/imgs/lu-8.png)

2. In your character sheet, increase your `Max HP` by the final amount rolled:
 
![](/imgs/lu-hp-2.png)

### Add New Class Features
9. Navigate to the `Compendium Packs` tab and find the `Class Features (SRD)` set:

![](/imgs/lu-9.png)

10. For each new class feature you gain, search for the feature name and Import it so it shows up in the `Items Directory`:

![](/imgs/lu-10.png)

11. Drag the new class feature into your character sheet:

![](/imgs/lu-11.png)

### Spells
1. If you gain spells known or new spell slot levels, you may need to pick new spells (depending on how your class prepares spells). Refer to the Players Handbook for details.

2. If you need to add spells, add them in the same manner as Class Features:
 
![](/imgs/lu-spells-2.png)

### Misc Resources
1. Some classes track specific usage of resources, like the Paladin's Lay of Hands feature, which can be used to restore a number of Hit Points equal to your Paladin level times five. Update or add any new tracked resources, marking whether they get restored on a short or long rest as needed:

![](/imgs/lu-misc-1.png) 

### Ability Score Improvements
1. Certain levels grant an Ability Score Improvement. For this, you can either add 2 points to any ability scores (up to the allowed maximum of 20 in any one score), or pick a feat. Simple increment any two ability scores by one, or the same one twice in your character sheet. Feats are added in the same manner as class features.

## Multiclassing
Coming soon!
