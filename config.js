window.$docsify = {
    el: "#app",
    loadSidebar: true,
    homepage: "home.md",
    notFoundPage: true,
    logo: "wizard-face.png",
    name: "The Wizard",
    toc: {
        scope: '.markdown-section',
        headings: 'h1, h2, h3',
        title: 'Table of Contents',
    },    
}
