# The Wizard

![wizard-face.png](wizard-face.png)

Live page at https://wizard.thetwitchy.com.

## Description
This page and repo documents how to perform common D&D 5E player tasks in Foundry VTT, such as buying equipment and leveling up. It's intended to be used as a part of the Sword Coast Misadventures campaign, but other DMs may find it helpful either as-is, or as a starting point for hosting thier own documentation.

## Self Hosting
This repo can be served as-is with no backend systems or additional processing needed, as it uses [Docsify.js](https://docsify.js.org) which automatically renders Markdown to the browser. This means that a simple HTTP server like nginx or Apache can serve this repo directly (just copy and paste the entire repo into your `/var/www` folder). It can also probably be deployed directly with Gitlab Pages or Github Pages. DMs wanting to host a modified version can simply Fork this repo into thier own space, modify it, and then host as needed.

## Current Hosting
Currently the live page is deployed to a Dokku instance via the [nginx Static buildpack](https://github.com/dokku/buildpack-nginx). This happens automatically as a part of the CI/CD process, whenever pushes are made to the `master` branch.
