# How to Buy Equipment

1. First, you must be at a place or area where you can buy equipment, and the shopkeeper is willing and able to sell to you.

2. Modify your currency amounts according to the item and amount purchased.

3. Next, Click the `Compendium` tab:

![](/imgs/be-2.png)

4. Navigate to `Items (SRD)`: 

![](/imgs/be-3.png)

5. Search for the bought item:

![](/imgs/be-4.png)

6. Import the item:

![](/imgs/be-5.png)

7. Find the imported item from the `Items Directory` tab:

![](/imgs/be-6.png)

8. Drag the item from the `Items Directory` tab into your open character sheet:

![](/imgs/be-7.png)

9. Visit your inventory to verify the item was added:

![](/imgs/be-8.png)

