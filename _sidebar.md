* [Home](/ "The Wizard - Home")
* How to...
    * [Buy Equipment](buying-equipment.md "The Wizard - Buying Equipment")
    * [Level Up](level-up.md "The Wizard - Leveling Up")
    * [Add a Missing Object](missing-object.md "The Wizard - Adding Missing Object")
