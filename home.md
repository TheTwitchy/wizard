# The Wizard
This documents how to perform common player tasks in the Foundry D&D 5E system. It is intended as a companion to the Sword Coast Misadventures campaign, but may be helpful for others.

## Schedule
* 7/31 - Off!
* 8/7 - D&D
* 8/14 - D&D
* 8/21 - D&D
* 8/28 - Off!
* 9/4 - D&D
* 9/11 - D&D
* 9/18 - D&D
